package ru.kolesnikov.tm.repository;

import ru.kolesnikov.tm.api.repository.IProjectRepository;
import ru.kolesnikov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId, Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        final List<Project> projects = findAll(userId);
        return projects.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name)  {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void add(final List<Project> projects) {
        for (final Project project: projects) add(project);
    }

    @Override
    public void add(Project... projects) {
        for (final Project project: projects) add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public void load(List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void load(Project... project) {
        clear();
        add(projects);
    }

    @Override
    public List<Project> getProjectList() {
        return projects;
    }

}