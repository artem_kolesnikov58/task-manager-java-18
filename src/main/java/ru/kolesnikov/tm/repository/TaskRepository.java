package ru.kolesnikov.tm.repository;

import ru.kolesnikov.tm.api.repository.ITaskRepository;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task: tasks) {
           if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void add(final List<Task> tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void add(final Task... tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void load(final List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        add(tasks);
    }

    @Override
    public List<Task> getTaskList() {
        return tasks;
    }

}