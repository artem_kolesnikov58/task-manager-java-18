package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.bootstrap.Bootstrap;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public final class ArgumentCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String commandName() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ARGUMENTS]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command : commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
        System.out.println("[OK]");
    }

}
