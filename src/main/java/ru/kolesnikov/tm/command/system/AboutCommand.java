package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String commandName() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Artem Kolesnikov");
        System.out.println("E-MAIL: tema58-rus@yandex.ru");
    }

}
