package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.bootstrap.Bootstrap;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ProgramHelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String commandName() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommandService().getCommandList();
        for (AbstractCommand command : commands) {
            System.out.println(command.commandName() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}