package ru.kolesnikov.tm.command.data.xml.fasterXml;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataXmlFasterXmlClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-xml-fx-clear";
    }

    @Override
    public String description() {
        return "Remove XML fasterXML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE XML FASTERXML FILE]");
        final File file = new File(DataConstant.FILE_XML_FX);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}