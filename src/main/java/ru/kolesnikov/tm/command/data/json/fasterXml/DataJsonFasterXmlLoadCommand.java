package ru.kolesnikov.tm.command.data.json.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-json-fx-load";
    }

    @Override
    public String description() {
        return "Load data from json fasterXML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON FASTERXML LOAD]");
        final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON_FX)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(jsonData, Domain.class);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}