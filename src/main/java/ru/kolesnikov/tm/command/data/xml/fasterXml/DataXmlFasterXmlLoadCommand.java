package ru.kolesnikov.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-xml-fx-load";
    }

    @Override
    public String description() {
        return "Load data from XML fasterXML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML FASTERXML LOAD]");
        final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        final XmlMapper xmlMapper = new XmlMapper();
        final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}