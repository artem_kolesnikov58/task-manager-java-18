package ru.kolesnikov.tm.command.data.binary;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}