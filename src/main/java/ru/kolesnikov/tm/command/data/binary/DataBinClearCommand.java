package ru.kolesnikov.tm.command.data.binary;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataBinClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}