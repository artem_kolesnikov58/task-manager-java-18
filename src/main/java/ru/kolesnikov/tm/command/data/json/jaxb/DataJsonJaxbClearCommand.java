package ru.kolesnikov.tm.command.data.json.jaxb;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataJsonJaxbClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-json-jb-clear";
    }

    @Override
    public String description() {
        return "Remove json jax-b file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE JSON JAX-B FILE]");
        final File file = new File(DataConstant.FILE_JSON_JB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}