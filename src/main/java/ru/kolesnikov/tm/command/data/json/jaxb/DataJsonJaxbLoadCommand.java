package ru.kolesnikov.tm.command.data.json.jaxb;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonJaxbLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-json-jb-load";
    }

    @Override
    public String description() {
        return "Load data from json jax-b file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON JAX-B LOAD]");

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final File file = new File(DataConstant.FILE_JSON_JB);
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}