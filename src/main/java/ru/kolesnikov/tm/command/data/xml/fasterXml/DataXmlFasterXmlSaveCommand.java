package ru.kolesnikov.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataXmlFasterXmlSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-xml-fx-save";
    }

    @Override
    public String description() {
        return "Save data to XML fasterXML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML FASTERXML SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConstant.FILE_XML_FX);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final XmlMapper xmlMapper = new XmlMapper();
        final String xmlData = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xmlData.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}