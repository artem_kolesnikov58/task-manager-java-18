package ru.kolesnikov.tm.command.data.xml.jaxb;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "data-xml-jb-load";
    }

    @Override
    public String description() {
        return "Load data from XML jax-b file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML JAX-B LOAD]");

        final File file = new File(DataConstant.FILE_XML_JB);
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}