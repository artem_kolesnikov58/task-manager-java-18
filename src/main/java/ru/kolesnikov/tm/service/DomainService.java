package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.service.IDomainService;
import ru.kolesnikov.tm.api.service.IProjectService;
import ru.kolesnikov.tm.api.service.ITaskService;
import ru.kolesnikov.tm.api.service.IUserService;
import ru.kolesnikov.tm.dto.Domain;

public final class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public DomainService(
            final ITaskService taskService,
            final IProjectService projectService,
            final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.getTaskList());
        domain.setProjects(projectService.getProjectList());
        domain.setUsers(userService.findAll());
    }

}